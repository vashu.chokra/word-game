#include<iostream>
#include<map>

using namespace std;

class TrieNode
{
	public:
    map<char, TrieNode*> children;
    bool endOfWord;
	TrieNode()
	{
        endOfWord = false;
    }
};

class Trie 
{
	public:
    TrieNode *root;
    
	Trie()
	{
        root = new TrieNode();
    }

    void insert(string word)
	{
        TrieNode *current = root;
        for (int i = 0; i < word.size(); i++)
		{
            char ch = word[i];
            TrieNode *node = current->children[ch];
            if (node == NULL)
			{
                node = new TrieNode();
                current->children[ch] = node;
            }
            current = node;
        }
        current->endOfWord = true;
    }

    bool search(string word) {
        TrieNode *current = root;
        for (int i = 0; i < word.size(); i++) {
            char ch = word[i];
            TrieNode *node = current->children[ch];
            if (node == NULL) {
                return false;
            }
            current = node;
        }
        return current->endOfWord;
    }

    void deleteNode(string word)
	{
        deleteNode(root, word, 0);
    }

    bool deleteNode(TrieNode *current, string word, int index)
	{
        if (index == word.size())
		{
            if (!current->endOfWord)
			{
                return false;
            }
            current->endOfWord = false;
            return current->children.size() == 0;
        }
        char ch = word[index];
        TrieNode *node = current->children[ch];
        if (node == NULL)
		{
            return false;
        }
        bool shouldDeleteCurrentNode = deleteNode(node, word, index + 1);
        if (shouldDeleteCurrentNode)
		{
            current->children.erase(ch);
            return current->children.size() == 0;
        }
        return false;
    }
};

